
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceWarehouse
{
    /// <summary>
    /// 【类】PurchaseOrderSub,【描述】订单明细
    /// </summary> 
    public class PurchaseOrderSub 
    {
        
        /// <summary>
        /// 【属性】ID,【描述】
        /// </summary>     
        public int Id {get;set;}

        /// <summary>
        /// 【属性】订单ID,【描述】
        /// </summary>     
        public int tid {get;set;}

        /// <summary>
        /// 【属性】商品编号,【描述】
        /// </summary>     
        public int spid {get;set;}

        /// <summary>
        /// 【属性】商品图片,【描述】
        /// </summary>     
        public string sptp {get;set;}

        /// <summary>
        /// 【属性】商品名称,【描述】
        /// </summary>     
        public string spmc {get;set;}

        /// <summary>
        /// 【属性】报货数,【描述】
        /// </summary>     
        public int bhs {get;set;}

        /// <summary>
        /// 【属性】报货单价,【描述】
        /// </summary>     
        public double bhdj {get;set;}

        /// <summary>
        /// 【属性】报货总额,【描述】
        /// </summary>     
        public double bhje {get;set;}

        /// <summary>
        /// 【属性】采购数,【描述】
        /// </summary>     
        public double cgs {get;set;}

        /// <summary>
        /// 【属性】采购单价,【描述】
        /// </summary>     
        public double cgdj {get;set;}

        /// <summary>
        /// 【属性】采购总价,【描述】
        /// </summary>     
        public double cgje {get;set;}

        /// <summary>
        /// 【属性】是否收货,【描述】
        /// </summary>     
        public bool sfsh {get;set;}

        /// <summary>
        /// 【属性】收货时间,【描述】
        /// </summary>     
        public string shsj {get;set;}

        /// <summary>
        /// 【属性】商品条码Id,【描述】
        /// </summary>     
        public int sptmid {get;set;}

        /// <summary>
        /// 【属性】系统商品条码,【描述】
        /// </summary>     
        public long xtsptm {get;set;}

        /// <summary>
        /// 【属性】系统商品条码,【描述】
        /// </summary>     
        public int ggid {get;set;}

        /// <summary>
        /// 【属性】规格型号,【描述】
        /// </summary>     
        public string ggxh {get;set;}

        /// <summary>
        /// 【属性】计量单位对应,【描述】
        /// </summary>     
        public string jldw {get;set;}

        /// <summary>
        /// 【属性】规格序号(从0开始，0表示基准单位),【描述】
        /// </summary>     
        public int ggxuh {get;set;}

        /// <summary>
        /// 【属性】商品重量,【描述】
        /// </summary>     
        public double spzl {get;set;}

        /// <summary>
        /// 【属性】商品体积,【描述】
        /// </summary>     
        public double sptj {get;set;}

        /// <summary>
        /// 【属性】单位换算率,【描述】
        /// </summary>     
        public double dwhsl {get;set;}

    }
}