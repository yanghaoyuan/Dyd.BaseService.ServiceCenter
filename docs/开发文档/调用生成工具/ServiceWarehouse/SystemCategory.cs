
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceWarehouse
{
    /// <summary>
    /// 【类】系统商品类目,【描述】
    /// </summary> 
    public class SystemCategory 
    {
        
        /// <summary>
        /// 【属性】类目id,【描述】
        /// </summary>     
        public int id {get;set;}

        /// <summary>
        /// 【属性】类名序号,【描述】
        /// </summary>     
        public string lmxh {get;set;}

        /// <summary>
        /// 【属性】类名名称,【描述】
        /// </summary>     
        public string lmmc {get;set;}

        /// <summary>
        /// 【属性】是否冻结,【描述】
        /// </summary>     
        public bool sfdj {get;set;}

        /// <summary>
        /// 【属性】上级类目序号,【描述】
        /// </summary>     
        public int sjlmid {get;set;}

        /// <summary>
        /// 【属性】类目路径,【描述】
        /// </summary>     
        public string lmlj {get;set;}

        /// <summary>
        /// 【属性】备注,【描述】
        /// </summary>     
        public string bz {get;set;}

        /// <summary>
        /// 【属性】末级,【描述】
        /// </summary>     
        public bool mj {get;set;}

        /// <summary>
        /// 【属性】级别,【描述】
        /// </summary>     
        public int jb {get;set;}

        /// <summary>
        /// 【属性】类目顺序,【描述】
        /// </summary>     
        public int sx {get;set;}

        /// <summary>
        /// 【属性】建档人编码,【描述】
        /// </summary>     
        public string cjrbm {get;set;}

        /// <summary>
        /// 【属性】建档人名称,【描述】
        /// </summary>     
        public string cjrmc {get;set;}

        /// <summary>
        /// 【属性】建档日期,【描述】
        /// </summary>     
        public string cjrq {get;set;}

        /// <summary>
        /// 【属性】变更人编码,【描述】
        /// </summary>     
        public string bgrbm {get;set;}

        /// <summary>
        /// 【属性】变更人名称,【描述】
        /// </summary>     
        public string bgrmc {get;set;}

        /// <summary>
        /// 【属性】变更日期,【描述】
        /// </summary>     
        public string bgrq {get;set;}

    }
}