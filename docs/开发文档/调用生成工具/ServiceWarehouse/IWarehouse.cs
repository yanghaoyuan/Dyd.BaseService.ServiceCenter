
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceWarehouse
{
    /// <summary>
    /// 【服务】AutoServiceDemo,【描述】AutoServiceDemo
    /// </summary>
    public class IWarehouseClient:XXF.BaseService.ServiceCenter.Client.RemoteClient.ThriftRemoteClient
    {
        public IWarehouseClient()
        {
        }

        
        /// <summary>
        /// 【方法】GetPurchaseOrderMainList,【描述】获取订单列表
        /// </summary>
           
        public List<PurchaseOrderMain> GetPurchaseOrderMainList(string beginDate,string endDate,int dqbm,int pageIndex,int pageSize)
        {
            return Call<List<PurchaseOrderMain>>("GetPurchaseOrderMainList",new object[]{beginDate,endDate,dqbm,pageIndex,pageSize}); 
        }

        /// <summary>
        /// 【方法】GetSystemGoodList,【描述】获取所有系统商品
        /// </summary>
           
        public List<SystemGood> GetSystemGoodListByIds(string categoryIds)
        {
            return Call<List<SystemGood>>("GetSystemGoodListByIds",new object[]{categoryIds}); 
        }

        /// <summary>
        /// 【方法】GetShopList,【描述】获取商家列表
        /// </summary>
           
        public List<Shop> GetShopList(int dqbm)
        {
            return Call<List<Shop>>("GetShopList",new object[]{dqbm}); 
        }

        /// <summary>
        /// 【方法】GetShopList,【描述】获取商家列表
        /// </summary>
           
        public List<SystemGood> GetSystemGoodList(string date,int size)
        {
            return Call<List<SystemGood>>("GetSystemGoodList",new object[]{date,size}); 
        }

        /// <summary>
        /// 【方法】GetShopList,【描述】获取系统商品类名
        /// </summary>
           
        public List<SystemCategory> GetSystemCategoryList(string date,int size)
        {
            return Call<List<SystemCategory>>("GetSystemCategoryList",new object[]{date,size}); 
        }

        /// <summary>
        /// 【方法】GetSystemBarcodeList,【描述】获取系统商品规格
        /// </summary>
           
        public List<SystemBarcode> GetSystemBarcodeList(string date,int size)
        {
            return Call<List<SystemBarcode>>("GetSystemBarcodeList",new object[]{date,size}); 
        }

       
    }
}
