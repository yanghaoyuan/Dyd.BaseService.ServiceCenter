
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceWarehouse
{
    /// <summary>
    /// 【类】SystemGood,【描述】系统商品
    /// </summary> 
    public class SystemGood 
    {
        
        /// <summary>
        /// 【属性】商品Id,【描述】
        /// </summary>     
        public int spid {get;set;}

        /// <summary>
        /// 【属性】类目序号,【描述】
        /// </summary>     
        public int lmxh {get;set;}

        /// <summary>
        /// 【属性】商品条码,【描述】
        /// </summary>     
        public long sptm {get;set;}

        /// <summary>
        /// 【属性】商品名称,【描述】
        /// </summary>     
        public string spmc {get;set;}

        /// <summary>
        /// 【属性】商品副标题,【描述】
        /// </summary>     
        public string spfbt {get;set;}

        /// <summary>
        /// 【属性】商品助记,【描述】
        /// </summary>     
        public string spzj {get;set;}

        /// <summary>
        /// 【属性】商品信息,【描述】
        /// </summary>     
        public string spxx {get;set;}

        /// <summary>
        /// 【属性】商品图文,【描述】
        /// </summary>     
        public string sptw {get;set;}

        /// <summary>
        /// 【属性】是否冻结,【描述】
        /// </summary>     
        public bool sfdj {get;set;}

        /// <summary>
        /// 【属性】商品主图,【描述】
        /// </summary>     
        public string sptp {get;set;}

        /// <summary>
        /// 【属性】商品附图,【描述】
        /// </summary>     
        public string spft {get;set;}

        /// <summary>
        /// 【属性】条码ID,【描述】
        /// </summary>     
        public int sptmid {get;set;}

        /// <summary>
        /// 【属性】商品图片修改时间,【描述】
        /// </summary>     
        public string sptpxgsj {get;set;}

        /// <summary>
        /// 【属性】建档日期,【描述】
        /// </summary>     
        public string jdrq {get;set;}

        /// <summary>
        /// 【属性】建档人编码,【描述】
        /// </summary>     
        public string jdrbm {get;set;}

        /// <summary>
        /// 【属性】建档人名称,【描述】
        /// </summary>     
        public string jdrmc {get;set;}

        /// <summary>
        /// 【属性】变更日期,【描述】
        /// </summary>     
        public string bgrq {get;set;}

        /// <summary>
        /// 【属性】变更人编码,【描述】
        /// </summary>     
        public string bgrbm {get;set;}

        /// <summary>
        /// 【属性】变更人名称,【描述】
        /// </summary>     
        public string bgrmc {get;set;}

        /// <summary>
        /// 【属性】是否删除,【描述】
        /// </summary>     
        public bool sfsc {get;set;}

        /// <summary>
        /// 【属性】gx,【描述】功效
        /// </summary>     
        public string gx {get;set;}

        /// <summary>
        /// 【属性】td,【描述】特点
        /// </summary>     
        public string td {get;set;}

        /// <summary>
        /// 【属性】spgjz,【描述】商品关键字
        /// </summary>     
        public string spgjz {get;set;}

        /// <summary>
        /// 【属性】spzj,【描述】基准单位(0)个(1)克
        /// </summary>     
        public int jzdw {get;set;}

    }
}