
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceWarehouse
{
    /// <summary>
    /// 【类】SystemBarcode,【描述】商品规格
    /// </summary> 
    public class SystemBarcode 
    {
        
        /// <summary>
        /// 【属性】规格ID,【描述】
        /// </summary>     
        public int ggid {get;set;}

        /// <summary>
        /// 【属性】商品条码,【描述】
        /// </summary>     
        public long sptm {get;set;}

        /// <summary>
        /// 【属性】是否基准单位,【描述】规格序号(从0开始，0表示基准单位)
        /// </summary>     
        public int ggxh {get;set;}

        /// <summary>
        /// 【属性】规格描述,【描述】
        /// </summary>     
        public string spgg {get;set;}

        /// <summary>
        /// 【属性】商品单位,【描述】
        /// </summary>     
        public string spdw {get;set;}

        /// <summary>
        /// 【属性】商品重量(KG),【描述】
        /// </summary>     
        public double spzl {get;set;}

        /// <summary>
        /// 【属性】商品体积(m3),【描述】
        /// </summary>     
        public double sptj {get;set;}

        /// <summary>
        /// 【属性】单位换算率,【描述】
        /// </summary>     
        public double dwhsl {get;set;}

        /// <summary>
        /// 【属性】是否冻结,【描述】
        /// </summary>     
        public int sfdj {get;set;}

        /// <summary>
        /// 【属性】建档日期,【描述】
        /// </summary>     
        public string jdrq {get;set;}

        /// <summary>
        /// 【属性】建档人编码,【描述】
        /// </summary>     
        public string jdrbm {get;set;}

        /// <summary>
        /// 【属性】建档人名称,【描述】
        /// </summary>     
        public string jdrmc {get;set;}

        /// <summary>
        /// 【属性】变更日期,【描述】
        /// </summary>     
        public string bgrq {get;set;}

        /// <summary>
        /// 【属性】变更人编码,【描述】
        /// </summary>     
        public string bgrbm {get;set;}

        /// <summary>
        /// 【属性】变更人名称,【描述】
        /// </summary>     
        public string bgrmc {get;set;}

        /// <summary>
        /// 【属性】是否废弃,【描述】0正常，1废弃
        /// </summary>     
        public bool sffq {get;set;}

    }
}