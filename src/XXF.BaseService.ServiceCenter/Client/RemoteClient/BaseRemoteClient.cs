﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XXF.BaseService.ServiceCenter.Client.RemoteClient
{
    /// <summary>
    /// 自定义远程客户端
    /// </summary>
    public class BaseRemoteClient
    {
        /// <summary>
        /// 客户端模拟调用
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="methodname"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public virtual T Call<T>(string methodname,object[] param)
        {
            return default(T);
        }
    }
}
