using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XXF.Extensions;
using XXF.Db;
using XXF.BaseService.ServiceCenter.Model;
using XXF.BaseService.ServiceCenter.SystemRuntime;

namespace XXF.BaseService.ServiceCenter.Dal
{
    /*代码自动生成工具自动生成,不要在这里写自己的代码，否则会被自动覆盖哦 - 车毅*/
    public partial class tb_node_dal
    {
        public virtual bool UpdateRunState(DbConn PubConn, long maxtimeoutsenconds)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>()
            {
            };
            Par.Add(new ProcedureParameter("@maxtimeoutsenconds", maxtimeoutsenconds));
            int rev = PubConn.ExecuteSql("update tb_node set runstate=" + (int)EnumRunState.Stop + "where  DATEDIFF(S,nodeheartbeattime,getdate())>@maxtimeoutsenconds", Par);
            return rev == 1;
        }

        public virtual bool UpdateLastUpdateTime(DbConn PubConn, long sessionid)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>()
            {
            };
            Par.Add(new ProcedureParameter("@sessionid", sessionid));

            int rev = PubConn.ExecuteSql("update tb_node set nodeheartbeattime=getdate() where sessionid=@sessionid", Par);
            return rev == 1;

        }

        public virtual bool UpdatePerformace(DbConn PubConn, tb_node_model model)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>()
            {
                    
                    //服务id
                    new ProcedureParameter("@serviceid",    model.serviceid),
                    //会话id
                    new ProcedureParameter("@sessionid",    model.sessionid),
                    //运行状态
                    new ProcedureParameter("@runstate",    model.runstate),
                    //错误数量
                    new ProcedureParameter("@errorcount",    model.errorcount),
                    //访问数量
                    new ProcedureParameter("@connectioncount",    model.connectioncount),
                    //
                    new ProcedureParameter("@visitcount",    model.visitcount),
                    //线程数
                    new ProcedureParameter("@processthreadcount",    model.processthreadcount),
                    //cpu时间
                    new ProcedureParameter("@processcpuper",    model.processcpuper),
                    //内存大小
                    new ProcedureParameter("@memorysize",    model.memorysize),
                    ////文件大小
                    //new ProcedureParameter("@filesize",    model.filesize),
            };
            Par.Add(new ProcedureParameter("@id", model.id));

            int rev = PubConn.ExecuteSql("update tb_node set runstate=@runstate,errorcount=@errorcount,connectioncount=@connectioncount,visitcount=@visitcount,processthreadcount=@processthreadcount,processcpuper=@processcpuper,memorysize=@memorysize where serviceid=@serviceid and sessionid=@sessionid", Par);
            return rev == 1;

        }

        public virtual bool Add(DbConn PubConn, tb_node_model model)
        {

            List<ProcedureParameter> Par = new List<ProcedureParameter>()
                {
                    
                    //服务id
                    new ProcedureParameter("@serviceid",    model.serviceid),
                    //服务命名空间（服务唯一标示）
                    new ProcedureParameter("@servicenamespace",    model.servicenamespace),                    
                    //会话id
                    new ProcedureParameter("@sessionid",    model.sessionid),
                    //ip地址
                    new ProcedureParameter("@ip",    model.ip),
                    //端口
                    new ProcedureParameter("@port",    model.port),
                    //运行状态
                    new ProcedureParameter("@runstate",    model.runstate),
                    //节点心跳时间
                    //new ProcedureParameter("@nodeheartbeattime",    model.nodeheartbeattime),
                    //权重值
                    new ProcedureParameter("@boostpercent",    model.boostpercent),
                    //错误数量
                    new ProcedureParameter("@errorcount",    model.errorcount),
                    //访问数量
                    new ProcedureParameter("@connectioncount",    model.connectioncount),
                    //
                    new ProcedureParameter("@visitcount",    model.visitcount),
                    //线程数
                    new ProcedureParameter("@processthreadcount",    model.processthreadcount),
                    //cpu时间
                    new ProcedureParameter("@processcpuper",    model.processcpuper),
                    //内存大小
                    new ProcedureParameter("@memorysize",    model.memorysize),
                    //文件大小
                    new ProcedureParameter("@filesize",    model.filesize),
                    //创建时间
                    //new ProcedureParameter("@createtime",    model.createtime),
                    //接口版本号
                    new ProcedureParameter("@interfaceversion",    model.interfaceversion),
                    //协议json
                    new ProcedureParameter("@protocoljson",    model.protocoljson)   
                };
            int rev = PubConn.ExecuteSql(@"insert into tb_node(serviceid,servicenamespace,sessionid,ip,port,runstate,nodeheartbeattime,boostpercent,errorcount,connectioncount,visitcount,processthreadcount,processcpuper,memorysize,filesize,createtime,interfaceversion,protocoljson)
                                           values(@serviceid,@servicenamespace,@sessionid,@ip,@port,@runstate,getdate(),@boostpercent,@errorcount,@connectioncount,@visitcount,@processthreadcount,@processcpuper,@memorysize,@filesize,getdate(),@interfaceversion,@protocoljson)", Par);
            return rev == 1;

        }

        public virtual bool Edit(DbConn PubConn, tb_node_model model)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>()
            {
                    
                    //服务id
                    new ProcedureParameter("@serviceid",    model.serviceid),
                    //会话id
                    new ProcedureParameter("@sessionid",    model.sessionid),
                    //ip地址
                    new ProcedureParameter("@ip",    model.ip),
                    //端口
                    new ProcedureParameter("@port",    model.port),
                    //运行状态
                    new ProcedureParameter("@runstate",    model.runstate),
                    ////节点心跳时间
                    //new ProcedureParameter("@nodeheartbeattime",    model.nodeheartbeattime),
                    //权重值
                    new ProcedureParameter("@boostpercent",    model.boostpercent),
                    //错误数量
                    new ProcedureParameter("@errorcount",    model.errorcount),
                    //访问数量
                    new ProcedureParameter("@connectioncount",    model.connectioncount),
                    //
                    new ProcedureParameter("@visitcount",    model.visitcount),
                    //线程数
                    new ProcedureParameter("@processthreadcount",    model.processthreadcount),
                    //cpu时间
                    new ProcedureParameter("@processcpuper",    model.processcpuper),
                    //内存大小
                    new ProcedureParameter("@memorysize",    model.memorysize),
                    //文件大小
                    new ProcedureParameter("@filesize",    model.filesize),
                    ////创建时间
                    //new ProcedureParameter("@createtime",    model.createtime),
                    //接口版本号
                    new ProcedureParameter("@interfaceversion",    model.interfaceversion),
                    //协议json
                    new ProcedureParameter("@protocoljson",    model.protocoljson)
            };
            Par.Add(new ProcedureParameter("@id", model.id));

            int rev = PubConn.ExecuteSql("update tb_node set serviceid=@serviceid,ip=@ip,port=@port,runstate=@runstate,nodeheartbeattime=getdate(),boostpercent=@boostpercent,errorcount=@errorcount,connectioncount=@connectioncount,visitcount=@visitcount,processthreadcount=@processthreadcount,processcpuper=@processcpuper,memorysize=@memorysize,filesize=@filesize,createtime=getdate(),interfaceversion=@interfaceversion,protocoljson=@protocoljson where sessionid=@sessionid", Par);
            return rev == 1;

        }


        public virtual bool Delete(DbConn PubConn, long sessionid)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@sessionid", sessionid));

            string Sql = "delete from tb_node where sessionid=@sessionid";
            int rev = PubConn.ExecuteSql(Sql, Par);
            if (rev == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public virtual bool Delete(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id", id));

            string Sql = "delete from tb_node where id=@id";
            int rev = PubConn.ExecuteSql(Sql, Par);
            if (rev == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public virtual List<tb_node_model> GetAllListOnLine(DbConn PubConn, int serviceid, int maxtimeoutsenconds)
        {
            List<tb_node_model> rs = new List<tb_node_model>();
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@serviceid", serviceid));
            Par.Add(new ProcedureParameter("@maxtimeoutsenconds", maxtimeoutsenconds));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_node s where s.serviceid=@serviceid and DATEDIFF(S,nodeheartbeattime,getdate())<=@maxtimeoutsenconds");
            DataSet ds = new DataSet();
            PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    rs.Add(CreateModel(dr));
                }
            }
            return rs;
        }

        public virtual tb_node_model Get(DbConn PubConn, long sessionid)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@sessionid", sessionid));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_node s where s.sessionid=@sessionid");
            DataSet ds = new DataSet();
            PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return CreateModel(ds.Tables[0].Rows[0]);
            }
            return null;
        }

        public virtual tb_node_model Get(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id", id));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_node s where s.id=@id");
            DataSet ds = new DataSet();
            PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return CreateModel(ds.Tables[0].Rows[0]);
            }
            return null;
        }

        public virtual tb_node_model CreateModel(DataRow dr)
        {
            var o = new tb_node_model();

            //
            if (dr.Table.Columns.Contains("id"))
            {
                o.id = dr["id"].Toint();
            }
            //服务id
            if (dr.Table.Columns.Contains("serviceid"))
            {
                o.serviceid = dr["serviceid"].Toint();
            }
            //服务命名空间（服务唯一标示）
            if (dr.Table.Columns.Contains("servicenamespace"))
            {
                o.servicenamespace = dr["servicenamespace"].ToString();
            }
            //会话id
            if (dr.Table.Columns.Contains("sessionid"))
            {
                o.sessionid = dr["sessionid"].Tolong();
            }
            //ip地址
            if (dr.Table.Columns.Contains("ip"))
            {
                o.ip = dr["ip"].Tostring();
            }
            //端口
            if (dr.Table.Columns.Contains("port"))
            {
                o.port = dr["port"].Toint();
            }
            //运行状态
            if (dr.Table.Columns.Contains("runstate"))
            {
                o.runstate = dr["runstate"].ToByte();
            }
            //节点心跳时间
            if (dr.Table.Columns.Contains("nodeheartbeattime"))
            {
                o.nodeheartbeattime = dr["nodeheartbeattime"].ToDateTime();
            }
            //权重值
            if (dr.Table.Columns.Contains("boostpercent"))
            {
                o.boostpercent = dr["boostpercent"].Toint();
            }
            //错误数量
            if (dr.Table.Columns.Contains("errorcount"))
            {
                o.errorcount = dr["errorcount"].Tolong();
            }
            //访问数量
            if (dr.Table.Columns.Contains("connectioncount"))
            {
                o.connectioncount = dr["connectioncount"].Tolong();
            }
            //
            if (dr.Table.Columns.Contains("visitcount"))
            {
                o.visitcount = dr["visitcount"].Toint();
            }
            //线程数
            if (dr.Table.Columns.Contains("processthreadcount"))
            {
                o.processthreadcount = dr["processthreadcount"].Toint();
            }
            //cpu时间
            if (dr.Table.Columns.Contains("processcpuper"))
            {
                o.processcpuper = dr["processcpuper"].Todouble();
            }
            //内存大小
            if (dr.Table.Columns.Contains("memorysize"))
            {
                o.memorysize = dr["memorysize"].Todouble();
            }
            //文件大小
            if (dr.Table.Columns.Contains("filesize"))
            {
                o.filesize = dr["filesize"].Todouble();
            }
            //创建时间
            if (dr.Table.Columns.Contains("createtime"))
            {
                o.createtime = dr["createtime"].ToDateTime();
            }
            //接口版本号
            if (dr.Table.Columns.Contains("interfaceversion"))
            {
                o.interfaceversion = dr["interfaceversion"].Todouble();
            }
            //协议json
            if (dr.Table.Columns.Contains("protocoljson"))
            {
                o.protocoljson = dr["protocoljson"].ToString();
            }
            return o;
        }
    }
}