﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XXF.BaseService.ServiceCenter.SystemRuntime;
using XXF.Extensions;

namespace XXF.BaseService.ServiceCenter.Service.OpenDoc
{
    /// <summary>
    /// 服务方法公开文档
    /// </summary>
    public class MethodDocAttribute:OpenDocAttribute 
    {
        /// <summary>
        /// 参数描述
        /// </summary>
        public Dictionary<string, string> InputParamsText { get; set; }
        /// <summary>
        /// 返回参数描述
        /// </summary>
        public string ReturnParamText { get; set; }
        /// <summary>
        /// 作者
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Note { get; set; }

        public MethodDocAttribute() { }


        /// <summary>
        /// 开发接口文档描述
        /// </summary>
        /// <param name="methodname">接口方法名（中文简短）</param>
        /// <param name="methoddescription">接口方法描述</param>
        /// 参数描述 格式: 字段:字段注释;
        /// <param name="author">作者</param>
        /// <param name="version">创建时间</param>
        /// <param name="note">其他记录</param>
        public MethodDocAttribute(string text, string description,string inputparamstext, string returnparamtext, string author, string createtime, string note)
            : base(text, description)
        {
            Author = author;
            DateTime tdatetime;
            if (!string.IsNullOrWhiteSpace(createtime) && DateTime.TryParse(createtime,out tdatetime))
                CreateTime = tdatetime;

            InputParamsText = new Dictionary<string, string>();
            try
            {
                if (!string.IsNullOrWhiteSpace(inputparamstext))
                {
                    var ss = inputparamstext.Split(';');
                    foreach (var s in ss)
                    {
                        if (!string.IsNullOrWhiteSpace(s))
                        {
                            var ps = s.Split(':');
                            if (ps != null && ps.Length == 2)
                            { InputParamsText.Add(ps[0], ps[1]); }
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                throw new ServiceCenterException("MethodDocAttribute中inputparamstext参数格式不正确,解析失败;(参数描述格式: 字段:字段注释;)", exp);
            }
           
            ReturnParamText = returnparamtext;
            Note = note;
        }
    }
}
