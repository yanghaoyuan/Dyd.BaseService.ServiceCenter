using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace XXF.BaseService.ServiceCenter.Model
{
    /// <summary>
    /// tb_protocolversion Data Structure.
    /// </summary>
    [Serializable]
    public partial class tb_protocolversion_model
    {
	/*代码自动生成工具自动生成,不要在这里写自己的代码，否则会被自动覆盖哦 - 车毅*/
        
        /// <summary>
        /// 
        /// </summary>
        public int id { get; set; }
        
        /// <summary>
        /// 服务id
        /// </summary>
        public int serviceid { get; set; }
        
        /// <summary>
        /// 协议版本号
        /// </summary>
        public int version { get; set; }
        
        /// <summary>
        /// 接口版本号
        /// </summary>
        public double interfaceversion { get; set; }
        
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime createtime { get; set; }
        
        /// <summary>
        /// 协议json
        /// </summary>
        public String protocoljson { get; set; }
        
    }
}