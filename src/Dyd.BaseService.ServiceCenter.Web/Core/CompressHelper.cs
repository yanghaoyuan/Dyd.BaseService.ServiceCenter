﻿using SharpCompress.Archive;
using SharpCompress.Archive.Zip;
using SharpCompress.Common;
using SharpCompress.Compressor.Deflate;
using SharpCompress.Reader;
using SharpCompress.Writer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Dyd.BaseService.ServiceCenter.Web.Core
{
    /// <summary>
    /// 文件压缩帮助类库
    /// </summary>
    public class CompressHelper
    {
        /// <summary>
        /// 通用解压 支持rar,zip
        /// </summary>
        /// <param name="compressfilepath"></param>
        /// <param name="uncompressdir"></param>
        public static void UnCompress(string compressfilepath, string uncompressdir)
        {
            string ext = Path.GetExtension(compressfilepath).ToLower();
            if (ext == ".rar")
                UnRar(compressfilepath, uncompressdir);
            else if (ext == ".zip")
                UnZip(compressfilepath, uncompressdir);
        }

        /// <summary>
        /// 通用解压 支持rar,zip
        /// </summary>
        /// <param name="compressfilepath"></param>
        /// <param name="uncompressdir"></param>
        public static void Compress(string compressFilePath, string compressFileName)
        {
            string ext = Path.GetExtension(compressFileName).ToLower();
            if (ext == ".rar")
                UnRar(compressFilePath, compressFileName);
            else if (ext == ".zip")
                Zip(compressFilePath, compressFileName);
        }

        /// <summary>
        /// 解压rar
        /// </summary>
        /// <param name="compressfilepath"></param>
        /// <param name="uncompressdir"></param>
        private static void UnRar(string compressfilepath, string uncompressdir)
        {
            using (Stream stream = File.OpenRead(compressfilepath))
            {
                using (var reader = ReaderFactory.Open(stream))
                {
                    while (reader.MoveToNextEntry())
                    {
                        if (!reader.Entry.IsDirectory)
                        {
                            reader.WriteEntryToDirectory(uncompressdir, ExtractOptions.ExtractFullPath | ExtractOptions.Overwrite);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 解压zip
        /// </summary>
        /// <param name="compressfilepath"></param>
        /// <param name="uncompressdir"></param>
        private static void UnZip(string compressfilepath, string uncompressdir)
        {
            using (var archive = ArchiveFactory.Open(compressfilepath))
            {
                foreach (var entry in archive.Entries)
                {
                    if (!entry.IsDirectory)
                    {
                        entry.WriteToDirectory(uncompressdir, ExtractOptions.ExtractFullPath | ExtractOptions.Overwrite);
                    }
                }
            }
        }


        /// <summary>
        /// 压缩Zip
        /// </summary>
        /// <param name="compressFilePath">路径</param>
        /// <param name="compressFileName">压缩后文件名称</param>
        private static void Zip(string compressFilePath, string compressFileName)
        {
            using (var archive = ZipArchive.Create())
            {
                archive.AddAllFromDirectory(compressFilePath);
                //archive.WriteToDirectory
                CompressionInfo info = new CompressionInfo();
                info.DeflateCompressionLevel = CompressionLevel.Default;
                archive.SaveTo(compressFilePath.Trim('\\') + '\\' + compressFileName, CompressionType.Deflate);
            }
        }
    }
}