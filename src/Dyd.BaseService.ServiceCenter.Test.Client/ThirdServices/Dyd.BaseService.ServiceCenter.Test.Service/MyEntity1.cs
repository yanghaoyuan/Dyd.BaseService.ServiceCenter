
using System;
using System.Collections.Generic;
using System.Text;

namespace Dyd.BaseService.ServiceCenter.Test.Service
{
    /// <summary>
    /// 【类】测试实体,【描述】我的测试实体
    /// </summary> 
    public class MyEntity1 
    {
        
        /// <summary>
        /// 【属性】实体参数1,【描述】我的测试实体参数1
        /// </summary>     
        public int p1 {get;set;}

    }
}